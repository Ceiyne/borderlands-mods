import unrealsdk
from Mods import ModMenu
from Mods.ModMenu import EnabledSaveType

import sys
from os import path

# thanks to apple1417 for this pointer about sys.path
# https://github.com/apple1417/bl-sdk-mods/blob/a45fde7c5edc2224c15ac0a775076aacd3d40a0a/CommandExtensions/__init__.py#L33-L35
sys.path.append(path.dirname(__file__)) # add local directory to path to load ctypes.pyd
KLF_ACTIVATE = 0x00000001

import ctypes
from ctypes import wintypes

class KBChange(ModMenu.SDKMod):
    Name: str = "Keyboard Change"
    Author: str = "Ceiyne"
    Description: str = "Changes keyboard to English (United States) at startup, in order to avoid issues where other layouts cause problems"
    Version: str = "0.1.1"
    SupportedGames: ModMenu.Game = ModMenu.Game.BL2 | ModMenu.Game.TPS  # Either BL2 or TPS; bitwise OR'd together
    Types: ModMenu.ModTypes = ModMenu.ModTypes.Utility  # One of Utility, Content, Gameplay, Library; bitwise OR'd together
    SaveEnabledState: EnabledSaveType = EnabledSaveType.LoadWithSettings

    def Enable(self) -> None:
        super().Enable()

        # thanks to user CristiFati at https://stackoverflow.com/a/48574049
        locale_id_bytes = b"00000409"
        klid = ctypes.create_string_buffer(locale_id_bytes)
        user32_dll = ctypes.WinDLL("user32")
        kernel32_dll = ctypes.WinDLL("kernel32")
        LoadKeyboardLayout = user32_dll.LoadKeyboardLayoutA
        LoadKeyboardLayout.argtypes = [wintypes.LPCSTR, wintypes.UINT]
        LoadKeyboardLayout.restype = wintypes.HKL
        GetLastError = kernel32_dll.GetLastError
        GetLastError.restype = wintypes.DWORD

        klh = LoadKeyboardLayout(klid, KLF_ACTIVATE)
        unrealsdk.Log("[KBChange] {0:s} returned: 0x{1:016X}".format(LoadKeyboardLayout.__name__, klh))
        unrealsdk.Log("[KBChange] {0:s} returned: {1:d}".format(GetLastError.__name__, GetLastError()))


instance = KBChange()
ModMenu.RegisterMod(instance)
