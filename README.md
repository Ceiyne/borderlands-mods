# Borderlands Mods

This is a home for my Borderlands mods.  See https://bl-sdk.github.io/ for instructions on how to get up and running with the PythonSDK for Borderlands 2 / The Pre-Sequel modding in order to use these mods.

### Keyboard Change
Borderlands 2 and The Pre-Sequel have an issue where some keys do not work properly if your keyboard is set to the Microsoft Windows Japanese IME.  Some keys, such as Esc and Tab, seem to get pressed twice.  This causes you to be unable to get into menu screens because the menu will open and then immediately close due to the double Esc. This problem may occur with other keyboard layouts but I did not test any others.  The Keyboard Change mode automatically changes your keyboard layout to English (United States) when the game starts up so you don't need to remember to do it every time.

### Sprint Set-and-Forget
This mod remembers your movement mode (sprinting or walking): if you were sprinting the last time you moved, you will automatically start sprinting the next time you move. Pressing the Sprint key will switch back and forth between sprinting and walking at any time. There is a setting in Mod Options that controls whether you start out sprinting or walking when the game begins.
